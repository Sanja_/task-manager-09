package ru.karamyshev.taskmanager;

import ru.karamyshev.taskmanager.constant.ArgumentConst;
import ru.karamyshev.taskmanager.constant.MsgCommandConst;
import ru.karamyshev.taskmanager.constant.TerminalConst;
import ru.karamyshev.taskmanager.model.TerminalCommand;
import ru.karamyshev.taskmanager.repository.CommandRepository;
import ru.karamyshev.taskmanager.util.NumberUtil;

import java.util.Scanner;

public class Application implements NumberUtil {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        parsArgs(args);
        inputCommand();
    }

    private static void inputCommand() {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            parsCommand(command);
        }
    }

    private static void parsCommand(final String args) {
        validateArgs(args);
        String[] command = args.trim().split("\\s+");
        for (String arg : command) {
            chooseResponsCommand(arg);
        }
    }

    private static void parsArgs(final String... args) {
        validateArgs(args);
        for (String arg : args) {
            chooseResponsArg(arg.trim());
        }
    }

    private static void validateArgs(final String... args) {
        if (args != null || args.length > 0) {
            return;
        }
        System.out.println(MsgCommandConst.COMMAND_ABSENT);
    }

    private static void chooseResponsArg(String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT: showAbout(); break;
            case ArgumentConst.VERSION: showVersion(); break;
            case ArgumentConst.HELP: showHelp(); break;
            case ArgumentConst.INFO: showInfo(); break;
            case ArgumentConst.ARGUMENTS: showArguments(); break;
            case ArgumentConst.COMMANDS: showCommands(); break;
            default: System.out.println(MsgCommandConst.ARGS_N_FOUND);
        }
    }

    private static void chooseResponsCommand(String command) {
        switch (command) {
            case TerminalConst.ABOUT: showAbout(); break;
            case TerminalConst.VERSION: showVersion(); break;
            case TerminalConst.HELP: showHelp(); break;
            case TerminalConst.INFO: showInfo(); break;
            case TerminalConst.EXIT: exit(); break;
            case TerminalConst.ARGUMENTS: showArguments(); break;
            case TerminalConst.COMMANDS: showCommands(); break;
            default: System.out.println(MsgCommandConst.COMMAND_N_FOUND);
        }
    }

    private static void showVersion() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.8");
    }

    private static void showAbout() {
        System.out.println("\n [ABOUT]");
        System.out.println("NAME: Alexander Karamyshev");
        System.out.println("EMAIL: sanja_19.96@mail.ru");
    }

    private static void showHelp() {
        System.out.println("\n [HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

    private static void showCommands() {
        System.out.println("\n [COMMANDS]");
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        for (final String command : commands) System.out.println(command);
    }

    private static void showArguments() {
        System.out.println("\n [ARGUMENTS]");
        final String[] arguments = COMMAND_REPOSITORY.getArgs();
        for (final String argument : arguments) System.out.println(argument);
    }

    private static void showInfo() {
        System.out.println("\n [INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM (bytes): " + usedMemoryFormat);
    }

    private static void exit() {
        System.exit(0);
    }
}
