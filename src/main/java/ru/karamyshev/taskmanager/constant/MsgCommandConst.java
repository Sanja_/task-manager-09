package ru.karamyshev.taskmanager.constant;

public interface MsgCommandConst {

    String DESCRIPTION_HELP = "Display terminal commands.";

    String DESCRIPTION_VERSION = "Show version info.";

    String DESCRIPTION_ABOUT = "Show developer info.";

    String DESCRIPTION_EXIT = "Close application.";

    String DESCRIPTION_INFO = "Display information about system.";

    String DESCRIPTION_COMMAND = "Show program commands.";

    String DESCRIPTION_ARGUMENT = "Show program arguments.";

    String COMMAND_EMPTY = "\n Line is empty.";

    String COMMAND_N_FOUND = " \n Error! Command not found.";

    String ARGS_N_FOUND = " \n Error! Arguments not found.";

    String COMMAND_ABSENT = "\n Error! Command absent.";
}
