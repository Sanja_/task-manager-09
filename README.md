# Информация о проекте.
## Приложение "Task Manager"
Осуществляет вывод информации по введённой команде.

### Команды:
- help - доступные команды.
- about - информация о разработчике.
- version - версия приложения.
- info - информация о системе.
- exit - закрывает приложение.
- argument - все аргументы для запуска через консоль.
- commands - все команды приложения.

# Стек
- Java 8.
- IntelliJ IDEA.
- Maven 3.

# Аппаратное обеспечение.
Процессор: 
- Intel Core 2 Quad и выше.
- Amd Athlon 64 и выше. 

ОЗУ: 2гб.    

Графический память: 512 Мб.

Переферийные устройства: клавиатура, мышь.          
       
# Программное обеспечение.
- JDK 1.8.
- Windows 7.
- Maven 3.

# Сборка jar файла
``` 
mvn clean package 
```
# Запуск приложения.
 ```
 java -jar target/taskmanager-1.0.0.jar help version about info arg cmd
 ```

![](https://drive.google.com/uc?export=view&id=1SWghNCOOXYlvlgTk-RqQRHGeok7YEv4F)

### Ввод команд в консоль приложения

![](https://drive.google.com/uc?export=view&id=19Ek7piAn0bylORYvoHS3KFe-dOrs0mlL)

![](https://drive.google.com/uc?export=view&id=1kUmDgAlDw3pMAfjXO_3_8O9PyTWHKRzR)

# Разработчики.
**Имя**: Александр Карамышев.

**Телефон:** 8(800)-555-35-35.

**Email**: sanja_19.96@mail.ru.
